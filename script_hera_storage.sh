#!/bin/bash
mqttHost="192.168.1.12"

sizeYanik=`ssh root@hera "du -sk /mnt/Love/Julianik/Yanik" | awk '{print $1}'`
sizeJulien=`ssh root@hera "du -sk /mnt/Love/Julianik/Julien" | awk '{print $1}'`
sizeMultimedia=`ssh root@hera "du -sk /mnt/Love/Multimedia" | awk '{print $1}'`
sizeTopSecret=`ssh root@hera "du -sk /mnt/Love/Multimedia/TopSecret" | awk '{print $1}'`

sizeMultimediaAdjusted=`echo "$sizeMultimedia - $sizeTopSecret" | bc`
sizeTotal=`echo "$sizeYanik + $sizeJulien + $sizeMultimedia + $sizeTopSecret" | bc`

mqtt pub -h $mqttHost -t hera/storage/yanik/used -m $sizeYanik
mqtt pub -h $mqttHost -t hera/storage/julien/used -m $sizeJulien
mqtt pub -h $mqttHost -t hera/storage/multimedia/used -m "${sizeMultimediaAdjusted}"
mqtt pub -h $mqttHost -t hera/storage/topSecret/used -m $sizeTopSecret
mqtt pub -h $mqttHost -t hera/storage/used -m $sizeTotal
