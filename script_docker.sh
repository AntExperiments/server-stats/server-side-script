#!/bin/bash
mqttHost="192.168.1.12"

# CPU
# This measures the CPU usage three times (with one second wait between)
mqtt pub -h $mqttHost -t docker/cpu/usage	-m `mpstat 1 3 | awk 'END{print 100-$NF"%"}' | rev | cut -c 2- | rev`

# RAM
mqtt pub -h $mqttHost -t docker/ram/total	    -m `free | sed "2q;d" | awk '{ print $2 }'`
mqtt pub -h $mqttHost -t docker/ram/used	    -m `free | sed "2q;d" | awk '{ print $3 }'`
mqtt pub -h $mqttHost -t docker/ram/free	    -m `free | sed "2q;d" | awk '{ print $4 }'`
mqtt pub -h $mqttHost -t docker/ram/shared	    -m `free | sed "2q;d" | awk '{ print $5 }'`
mqtt pub -h $mqttHost -t docker/ram/cache	    -m `free | sed "2q;d" | awk '{ print $6 }'`
mqtt pub -h $mqttHost -t docker/ram/available	-m `free | sed "2q;d" | awk '{ print $7 }'`

# Swap
mqtt pub -h $mqttHost -t docker/swap/total	    -m `free | sed "3q;d" | awk '{ print $2 }'`
mqtt pub -h $mqttHost -t docker/swap/used	    -m `free | sed "3q;d" | awk '{ print $3 }'`
mqtt pub -h $mqttHost -t docker/swap/free	    -m `free | sed "3q;d" | awk '{ print $4 }'`


# Storage
rootStorageUsed=`df / | tail -n1 | awk '{print $3}'`
rootStorageFree=`df / | tail -n1 | awk '{print $4}'`
rootStorageTotal=`df / | tail -n1 | awk '{print $2}'`

containersRunning=`/snap/bin/docker container list | wc -l`
containersRunning=`echo "$containersRunning - 1" | bc`

mqtt pub -h $mqttHost -t docker/storage/root/used  -m $rootStorageUsed
mqtt pub -h $mqttHost -t docker/storage/root/free  -m $rootStorageFree
mqtt pub -h $mqttHost -t docker/storage/root/total -m $rootStorageTotal
mqtt pub -h $mqttHost -t docker/containers/running -m $containersRunning
