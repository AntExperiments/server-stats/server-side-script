#!/bin/bash
mqttHost="192.168.1.12"

# RAM
mqtt pub -h $mqttHost -t hera/ram/total	    -m `ssh root@hera "freecolor -o" | sed "2q;d" | awk '{ print $2 }'`
mqtt pub -h $mqttHost -t hera/ram/used	    -m `ssh root@hera "freecolor -o" | sed "2q;d" | awk '{ print $3 }'`
mqtt pub -h $mqttHost -t hera/ram/free	    -m `ssh root@hera "freecolor -o" | sed "2q;d" | awk '{ print $4 }'`
mqtt pub -h $mqttHost -t hera/ram/shared	    -m `ssh root@hera "freecolor -o" | sed "2q;d" | awk '{ print $5 }'`
mqtt pub -h $mqttHost -t hera/ram/cache	    -m `ssh root@hera "freecolor -o" | sed "2q;d" | awk '{ print $6 }'`
mqtt pub -h $mqttHost -t hera/ram/available	-m `ssh root@hera "freecolor -o" | sed "2q;d" | awk '{ print $7 }'`

# Swap
mqtt pub -h $mqttHost -t hera/swap/total	    -m `ssh root@hera "freecolor -o" | sed "3q;d" | awk '{ print $2 }'`
mqtt pub -h $mqttHost -t hera/swap/used	    -m `ssh root@hera "freecolor -o" | sed "3q;d" | awk '{ print $3 }'`
mqtt pub -h $mqttHost -t hera/swap/free	    -m `ssh root@hera "freecolor -o" | sed "3q;d" | awk '{ print $4 }'`
